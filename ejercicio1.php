<?php
/**
 * funcion que genera numeros aleatorios
 * @param int $minimo Valor minimo
 * @param int $maximo Valor maximo
 * @param int $numero numero de valores generado
 * @return int[] conjunto de numeros solicitado
 */
        
function ejercicio1($minimo,$maximo,$numero) {
    $local= array();
    
    for ($c=0;$c<$numero;$c++){
        $local[$c]= mt_rand($minimo,$maximo);
    }
    return $local;
}
$salida=ejercicio1(1,10,10);
var_dump($salida);


/*
 *salida del ejercicio:
 * C:\xampp\htdocs\phpalpe\ejercicios\hoja3\ejercicio1.php:13:
array (size=10)
  0 => int 9
  1 => int 7
  2 => int 4
  3 => int 3
  4 => int 3
  5 => int 1
  6 => int 10
  7 => int 3
  8 => int 7
  9 => int 9
 */    