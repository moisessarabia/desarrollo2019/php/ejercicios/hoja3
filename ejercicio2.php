<?php
/**
 * funcion que genera numeros aleatorios
 * @param int $minimo Valor minimo
 * @param int $maximo Valor maximo
 * @param int $numero numero de valores generado
 * @param int[] $salida es el array donde se almacena la salida
 * @return void 
 */
        
function ejercicio2($minimo,$maximo,$numero,&$salida) {
    /*
     * se inicializa el array para eliminar los numeros anteriores
     */
    $salida=array();
    
    /*
     * bucle que rellena el array
     */
    
    for ($c=0;$c<$numero;$c++){
        $salida[$c]= mt_rand($minimo,$maximo);
    }
   return $salida;
}
 
   $salida= ejercicio2(2,40,10,void);
   var_dump($salida);
   
/**
 * salida del ejercicio:
 * Fatal error: Only variables can be passed by reference in C:\xampp\htdocs\phpalpe\ejercicios\hoja3\ejercicio2.php on line 27;
 */


