<?php
/**
 * funcion que genera colores(Hex)
 * @param int $numero numero de colores a generar
 * @return array los colores solicitados en un array de strings
 */

function generacolores($numero) {
    $colores=array();
    for ($n=0;$n<$numero;$n++){
        $colores[$n]="#";
        for($c=1;$c<7;$c++){
            $colores[$n].= dechex(mt_rand(0,15));
        }
    }
    return $colores;
}
$colores= generacolores(16);
var_dump($colores);
/*
 * salida del ejercicio:
 * C:\xampp\htdocs\phpalpe\ejercicios\hoja3\ejercicio3.php:19:
array (size=16)
  0 => string '#315979' (length=7)
  1 => string '#c6850a' (length=7)
  2 => string '#1d9db0' (length=7)
  3 => string '#8f930c' (length=7)
  4 => string '#d0b019' (length=7)
  5 => string '#a1a1c8' (length=7)
  6 => string '#a9737a' (length=7)
  7 => string '#f8bb9f' (length=7)
  8 => string '#1f3020' (length=7)
  9 => string '#253057' (length=7)
  10 => string '#9b05c7' (length=7)
  11 => string '#018286' (length=7)
  12 => string '#5b42f5' (length=7)
  13 => string '#b7c4bd' (length=7)
  14 => string '#55219b' (length=7)
  15 => string '#56e054' (length=7)
 */